﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhotoEditorV2
{
    class ToolbarPanel
    {
        private Form1 form1res;
        private FlowLayoutPanel layout;
        private Panel parentpanel;
        private MainProcess mpref;


        /*ColorPicker*/
        private Color latestColorPicked;
        private bool colorPickerOpened = false;


        public ToolbarPanel(Form1 parent, FlowLayoutPanel layoutparent, Panel pp)
        {
            this.form1res = parent;
            this.layout = layoutparent;
            this.parentpanel = pp;
            menuSetup();
        }
        private void menuSetup()
        {
            latestColorPicked = Color.White;
            var values = Enum.GetValues(typeof(tool_type));
            int i = 0;
            foreach(tool_type t in values)
            {
                Button button = new Button();
                button.Height = 40;
                button.Width = 40;
                switch (t)
                {
                    case tool_type.Move:
                        button.BackgroundImage = Properties.Resources.move;
                        button.Click += new EventHandler(buttonMove_click);
                        button.Name = "movebutton";
                        break;
                    case tool_type.Paint:
                        button.BackgroundImage = Properties.Resources.brush;
                        button.Click += new EventHandler(buttonPaint_click);
                        button.Name = "paintbutton";
                        break;
                    case tool_type.Erase:
                        button.BackgroundImage = Properties.Resources.erase;
                        button.Click += new EventHandler(buttonErase_click);
                        button.Name = "erasebutton";
                        break;
                    case tool_type.Type:
                        button.BackgroundImage = Properties.Resources.font;
                        button.Click += new EventHandler(buttonType_click);
                        button.Name = "typebutton";
                        break;
                }
                button.BackgroundImageLayout = ImageLayout.Center;
                button.Tag = i;
                button.Margin = Padding.Empty;
                button.FlatStyle = FlatStyle.Flat;
                button.FlatAppearance.BorderSize = 0;
                layout.Controls.Add(button);
                i++;
            }

            Button colorbutton = new Button();
            colorbutton.Width = 40; colorbutton.Height = 100;
            colorbutton.Tag = "colorbutton";
            colorbutton.Name = "colorbutton";
            colorbutton.Click += new EventHandler(buttonColor_click);
            colorbutton.Dock = DockStyle.Bottom;
            colorbutton.BackColor = latestColorPicked;
            layout.Controls.Add(colorbutton);


            selectButton(tool_type.Move);
            form1res.setSelectedTool(tool_type.Move);
            
        }

        private void openColorPicker()
        {
            if (!colorPickerOpened)
            {
                colorPickerOpened = true;
                createColorPickerForm();
                
            }
            
        }

        private void createColorPickerForm()
        {
            
            ColorDialog cd = new ColorDialog();
            Button btn = layout.Controls.Find("colorbutton", true)[0] as Button;
            if (cd.ShowDialog() == DialogResult.OK)
            {
                
                btn.BackColor = cd.Color;
                latestColorPicked = cd.Color;
                mpref.Brush_color = cd.Color;
            }
            colorPickerOpened = false;

        }

        private void selectButton(tool_type t)
        {
            updateToolstrip(t);
            foreach (Control c in layout.Controls)
            {
                if (c.GetType() == typeof(Button))
                {
                    Button b = (Button)c;
                    switch (c.Name)
                    {
                        case "movebutton":
                            if (t == tool_type.Move) { b.BackColor = Color.Green; } else { b.BackColor = Color.FromArgb(149, 165, 166); }
                            break;
                        case "paintbutton":
                            if (t == tool_type.Paint) { b.BackColor = Color.Green; } else { b.BackColor = Color.FromArgb(149, 165, 166); }
                            break;
                        case "erasebutton":
                            if (t == tool_type.Erase) { b.BackColor = Color.Green; } else { b.BackColor = Color.FromArgb(149, 165, 166); }
                            break;
                        case "typebutton":
                            if (t == tool_type.Type) { b.BackColor = Color.Green; } else { b.BackColor = Color.FromArgb(149, 165, 166); }
                            break;
                    }
                }
            }
        }

        private void updateToolstrip(tool_type t)
        {
            switch (t)
            {
                case tool_type.Paint:
                    createPaintStrip();
                    break;
                case tool_type.Erase:
                    createEraseStrip();
                    break;
                case tool_type.Type:
                    createTypeStrip();
                    break;
                default:
                    Panel p = new Panel();
                    p.Name = "toolstrip";
                    p.Height = 40;
                    p.Dock = DockStyle.Fill;
                    p.Padding = Padding.Empty;
                    p.Margin = Padding.Empty;
                    updateStrip(p);
                    break;
            }
        }

        private void createPaintStrip()
        {
            Panel p = new Panel();
            p.Name = "toolstrip";
            p.Height = 40;
            p.Dock = DockStyle.Fill;
            p.Padding = Padding.Empty;
            p.Margin = Padding.Empty;

            FlowLayoutPanel flp = new FlowLayoutPanel();
            flp.Dock = DockStyle.Fill;
            flp.FlowDirection = FlowDirection.LeftToRight;
            flp.Padding = Padding.Empty;
            flp.Margin = Padding.Empty;

            Label labelbs = new Label();
            labelbs.Height = 40;
            labelbs.Width = 100;
            labelbs.Text = "Brush size: ";
            labelbs.Padding = new Padding(0, 10, 0, 0);
            labelbs.Font = new Font(labelbs.Font.FontFamily, 10);

            NumericUpDown brushSize = new NumericUpDown();
            brushSize.Height = 40;
            brushSize.Width = 50;
            brushSize.Minimum = 1;
            brushSize.Maximum = 999;
            brushSize.DecimalPlaces = 0;
            brushSize.Value = mpref.Brush_size;

            brushSize.Name = "numericupdownbrushsize";
            brushSize.Padding = new Padding(0, 10, 0, 0);
            brushSize.ValueChanged += BrushSize_ValueChanged;

            Label labelbo = new Label();
            labelbo.Height = 40;
            labelbo.Width = 100;
            labelbo.Text = "Brush opacity: ";
            labelbo.Padding = new Padding(0, 10, 0, 0);
            labelbo.Font = new Font(labelbs.Font.FontFamily, 10);

            TrackBar brushOpacity = new TrackBar();
            brushOpacity.Name = "trackbarbrushopactiy";
            brushOpacity.Height = 40;
            brushOpacity.Width = 100;
            brushOpacity.Minimum = 0;
            brushOpacity.Maximum = 255;
            brushOpacity.Value = mpref.Brush_opacity;
            brushOpacity.ValueChanged += BrushOpacity_ValueChanged;

            flp.Controls.Add(labelbs);
            flp.Controls.Add(brushSize);
            flp.Controls.Add(labelbo);
            flp.Controls.Add(brushOpacity);

            p.Controls.Add(flp);
            updateStrip(p);
        }
        
        private void createEraseStrip()
        {
            Panel p = new Panel();
            p.Name = "toolstrip";
            p.Height = 40;
            p.Dock = DockStyle.Fill;
            p.Padding = Padding.Empty;
            p.Margin = Padding.Empty;

            FlowLayoutPanel flp = new FlowLayoutPanel();
            flp.Dock = DockStyle.Fill;
            flp.FlowDirection = FlowDirection.LeftToRight;
            flp.Padding = Padding.Empty;
            flp.Margin = Padding.Empty;

            Label labelbs = new Label();
            labelbs.Height = 40;
            labelbs.Width = 100;
            labelbs.Text = "Erase size: ";
            labelbs.Padding = new Padding(0, 10, 0, 0);
            labelbs.Font = new Font(labelbs.Font.FontFamily, 10);

            NumericUpDown brushSize = new NumericUpDown();
            brushSize.Height = 40;
            brushSize.Width = 50;
            brushSize.Minimum = 1;
            brushSize.Maximum = 999;
            brushSize.DecimalPlaces = 0;
            brushSize.Value = mpref.Erase_size;

            brushSize.Name = "numericupdownerasesize";
            brushSize.Padding = new Padding(0, 10, 0, 0);
            brushSize.ValueChanged += EraseSize_ValueChanged;

           

            flp.Controls.Add(labelbs);
            flp.Controls.Add(brushSize);


            p.Controls.Add(flp);
            updateStrip(p);
        }

        private void createTypeStrip()
        {
            Panel p = new Panel();
            p.Name = "toolstrip";
            p.Height = 40;
            p.Dock = DockStyle.Fill;
            p.Padding = Padding.Empty;
            p.Margin = Padding.Empty;

            FlowLayoutPanel flp = new FlowLayoutPanel();
            flp.Dock = DockStyle.Fill;
            flp.FlowDirection = FlowDirection.LeftToRight;
            flp.Padding = Padding.Empty;
            flp.Margin = Padding.Empty;

            Label labelbs = new Label();
            labelbs.Height = 40;
            labelbs.Width = 100;
            labelbs.Text = "Font size: ";
            labelbs.Padding = new Padding(0, 10, 0, 0);
            labelbs.Font = new Font(labelbs.Font.FontFamily, 10);

            NumericUpDown typeSize = new NumericUpDown();
            typeSize.Height = 40;
            typeSize.Width = 50;
            typeSize.Minimum = 1;
            typeSize.Maximum = 999;
            typeSize.DecimalPlaces = 0;
            typeSize.Value = mpref.Type_size;

            typeSize.Name = "numericupdowntypesize";
            typeSize.Padding = new Padding(0, 10, 0, 0);
            typeSize.ValueChanged += TypeSize_ValueChanged;

            Label labelts = new Label();
            labelts.Height = 40;
            labelts.Width = 100;
            labelts.Text = "Font type: ";
            labelts.Padding = new Padding(0, 10, 0, 0);
            labelts.Font = new Font(labelbs.Font.FontFamily, 10);

            

            ComboBox fontSelect = new ComboBox();
            fontSelect.Height = 40;
            fontSelect.DropDownStyle = ComboBoxStyle.DropDownList;
            fontSelect.Name = "dropdownfonttype";

            List<string> fonts = new List<string>();
            FontFamily current = mpref.Font_type;

            foreach (FontFamily font in FontFamily.Families)
            {
                fonts.Add(font.Name);
            }

            fontSelect.Items.AddRange(fonts.ToArray());
            fontSelect.SelectedItem = current.Name;
            fontSelect.SelectedIndexChanged += FontSelect_SelectedIndexChanged;

            flp.Controls.Add(labelbs);
            flp.Controls.Add(typeSize);
            flp.Controls.Add(labelts);
            flp.Controls.Add(fontSelect);


            p.Controls.Add(flp);
            updateStrip(p);
        }

        private void updateStrip(Panel p)
        {
            Control c = null;
            
            foreach (Control cntrl in parentpanel.Controls)
            {
                if (cntrl.GetType() == typeof(Panel))
                {
                    if (cntrl.Name == "toolstrip")
                    {
                        c = cntrl;
                    }
                }
            }
            if(c == null)
            {
                parentpanel.Controls.Add(p);
            }
            else
            {
                parentpanel.Controls.Remove(parentpanel.Controls.Find("toolstrip", true)[0]);
                parentpanel.Controls.Add(p);
            }

            
            
        }

        public void SetMainProcess(MainProcess m)
        {
            mpref = m;
        }

        /*? ----EVENTS---- */


        private void buttonColor_click(object sender, EventArgs e)
        {
            openColorPicker();
        }
        // movebutton paintbutton erasebutton typebutton colorbutton
        private void buttonType_click(object sender, EventArgs e)
        {
            if (form1res.IsProjectOpenned())
            {
                selectButton(tool_type.Type);
                form1res.setSelectedTool(tool_type.Type);
            }
            
        }

        private void buttonErase_click(object sender, EventArgs e)
        {
            if (form1res.IsProjectOpenned())
            {
                selectButton(tool_type.Erase);
                form1res.setSelectedTool(tool_type.Erase);

            }
            
        }

        private void buttonPaint_click(object sender, EventArgs e)
        {
            if (form1res.IsProjectOpenned())
            {
                selectButton(tool_type.Paint);
                form1res.setSelectedTool(tool_type.Paint);
            }
            
        }

        private void buttonMove_click(object sender, EventArgs e)
        {
            if (form1res.IsProjectOpenned())
            {
                selectButton(tool_type.Move);
                form1res.setSelectedTool(tool_type.Move);
            }
            
        }

        private void BrushSize_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown n = sender as NumericUpDown;
            int value = Decimal.ToInt32(n.Value);
            mpref.Brush_size = value;
        }

        private void BrushOpacity_ValueChanged(object sender, EventArgs e)
        {
            TrackBar t = sender as TrackBar;
            mpref.Brush_opacity = t.Value;
        }

        private void EraseSize_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown n = sender as NumericUpDown;
            int value = Decimal.ToInt32(n.Value);
            mpref.Erase_size = value;
        }

        private void TypeSize_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown n = sender as NumericUpDown;
            int value = Decimal.ToInt32(n.Value);
            mpref.Type_size = value;
        }

        private void FontSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox c = sender as ComboBox;
            string font = c.SelectedItem as string;
            FontFamily value = new FontFamily(font);
            mpref.Font_type = value;
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhotoEditorV2
{
    class WindowToolbar
    {
        /*?REFERENCE*/
        private Form1 form1res;
        private FlowLayoutPanel layout;
        private MainProcess mpres;

        /*?GUILAYERMANAGER*/
        private List<Panel> panelList;
        //private Panel selectedPanel = null;

        public WindowToolbar(Form1 parent, FlowLayoutPanel layoutparent)
        {
            this.form1res = parent;
            this.layout = layoutparent;
            panelList = new List<Panel>();
            init();
        }

        private void init()// create newLayer button and deleteLayer button
        {
            layout.Click += Layout_Click;

            Panel ip = new Panel();
            ip.Width = layout.Width;
            ip.Height = 50;
            ip.Padding = new Padding(0, 0, 0, 5);

            Label lb = new Label();
            lb.Text = "Layers";
            lb.Left = layout.Width / 2 - lb.Width;
            ip.Controls.Add(lb);

            FlowLayoutPanel flp = new FlowLayoutPanel();
            flp.Dock = DockStyle.Bottom;
            flp.Height = 30;
     

            Button nl = new Button();
            nl.Height = 30;
            nl.Width = 30;
            nl.FlatStyle = FlatStyle.Flat;
            nl.FlatAppearance.BorderSize = 0;
            nl.BackgroundImage = Properties.Resources.page;
            nl.BackgroundImageLayout = ImageLayout.Center;
            nl.Click += NewLayer_Click;
            flp.Controls.Add(nl);

            Button dl = new Button();
            dl.Height = 30;
            dl.Width = 30;
            dl.FlatStyle = FlatStyle.Flat;
            dl.FlatAppearance.BorderSize = 0;
            dl.BackgroundImage = Properties.Resources.waste;
            dl.BackgroundImageLayout = ImageLayout.Center;
            dl.Click += DeleteLayer_Click;

            flp.Controls.Add(dl);
            
            FlowLayoutPanel flp2 = new FlowLayoutPanel();
            flp.Dock = DockStyle.Bottom;
            flp.Height = 30;
            flp.Padding = Padding.Empty;
            flp.Margin = Padding.Empty;
            flp.FlowDirection = FlowDirection.LeftToRight;

            Panel le = new Panel();
            le.Height = 30;
            le.Width = ip.Width;
            le.Padding = Padding.Empty;
            le.Margin = Padding.Empty;

            Label oplbl = new Label();
            oplbl.Text = "Opacity";
            oplbl.Width = 50;
            oplbl.Height = 30;
            oplbl.Dock = DockStyle.Left;

            TrackBar op = new TrackBar();
            op.Height = 30;
            op.Width = 150;
            op.Name = "trackbarlayeropacity";
            op.Minimum = 0;
            op.Maximum = 100;
            op.Enabled = false;
            
            op.ValueChanged += LayerOpacityTrackBar_valueChanged;
            op.EnabledChanged += LayerOpacity_EnabledChanged;
            //op.Margin = new Padding(0, 0, 20, 0);
            op.Margin = new Padding(10,0,10,0);
            
            
            le.Controls.Add(oplbl);
            le.Controls.Add(op);
            flp2.Controls.Add(le);
            

            layout.Controls.Add(ip);
            layout.Controls.Add(flp2);
            layout.Controls.Add(flp);
            
            

            
        }

        

        public void createNewLayerPanel(int id)
        {
            Panel nl = new Panel();
            nl.Name = "Layer" + panelList.Count;
            nl.Tag = id;// layerid
            nl.Width = layout.Width;
            nl.Height = 50;
            nl.Padding = new Padding(0, 0, 0, 5);
            nl.BackColor = Color.Gray;
            nl.Click += Nl_Click;

            Button hl = new Button(); // hide layer
            hl.Width = 25;
            hl.Height = 25;
            hl.Left = 10;
            hl.Top = 10;
            hl.Name = "hidebutton" + id;
            hl.FlatStyle = FlatStyle.Flat;
            hl.FlatAppearance.BorderSize = 0;
            hl.BackgroundImage = Properties.Resources.eye;
            hl.BackgroundImageLayout = ImageLayout.Stretch;
            hl.Click += Hl_Click;
            nl.Controls.Add(hl);

            PictureBox pp = new PictureBox(); // picture pre render
            pp.Width = 44;
            pp.Height = 44;
            pp.Left = 45;
            pp.Top = 3;
            pp.Name = "thumbnail"+id;
            pp.SizeMode = PictureBoxSizeMode.StretchImage;
            pp.BorderStyle = BorderStyle.FixedSingle;
            //pp.BackColor = Color.Gray;
            pp.Click += Nl_Click;
            nl.Controls.Add(pp);

            Label ln = new Label(); // layer name
            ln.Left = 105;
            ln.Top = (nl.Height / 2) - (ln.Height / 2);
            ln.Text = "Layer" + panelList.Count;
            ln.Click += Nl_Click;
            nl.Controls.Add(ln);

            layout.Controls.Add(nl);
            panelList.Add(nl);
            //updateSelectedLayer(nl);
        }

        public void deleteLayerPanel(int id)
        {
            for(int i = 0; i < panelList.Count; i++)
            {
                if((int)panelList[i].Tag == id)
                { 
                    
                    layout.Controls.Remove(panelList[i]);
                    panelList.Remove(panelList[i]);
                }
            }
        }

        public void newProject()
        {
            if(panelList.Count > 0)
            {
                foreach(Panel p in panelList)
                {
                    layout.Controls.Remove(p);
                }
                panelList.Clear();
            }
            else
            {
                panelList = new List<Panel>();
            }
        }

        public void setSelectedLayerPanel(int id)
        {
            if(panelList.Count > 0)
            {
                foreach (Panel p in panelList)
                {
                    if ((int)p.Tag == id)
                    {
                        p.BackColor = Color.LightBlue;
                        TrackBar tr = layout.Controls.Find("trackbarlayeropacity", true)[0] as TrackBar;
                        tr.Enabled = true;
                    }
                    else
                    {
                        p.BackColor = Color.Gray;
                    }
                }
            }
        }

        public void deselectLayerPanel()
        {
            if (panelList.Count > 0)
            {
                foreach (Panel p in panelList)
                {
                    p.BackColor = Color.Gray;
                    TrackBar tr = layout.Controls.Find("trackbarlayeropacity", true)[0] as TrackBar;
                    tr.Enabled = false;
                }
            }
        }

        public void updateLayerThumbnail(int id,Bitmap p)
        {
            if (panelList.Count > 0)
            {
                foreach(Panel pnl in panelList)
                {
                    if ((int)pnl.Tag == id)
                    {
                        PictureBox pn = pnl.Controls.Find("thumbnail"+id, true)[0] as PictureBox;
                        pn.Image = p;
                        p = null;
                        GC.Collect();
                        break;
                    }
                }
            }
        }

        public void updateLayerHide(int id, bool state)
        {
            if (panelList.Count > 0)
            {
                foreach (Panel p in panelList)
                {
                    if ((int)p.Tag == id)
                    {
                        Button bn = p.Controls.Find("hidebutton" + id, true)[0] as Button;
                        if (state) // ON
                        {
                            bn.BackgroundImage = Properties.Resources.eye;
                        }
                        else // OFF
                        {
                            bn.BackgroundImage = Properties.Resources.eyeclose;
                        }
                    }
                }
            }
        }

        public void addNewEffect(int lid,int eid,effect_type t)
        {
            if (panelList.Count > 0)
            {
                foreach (Panel p in panelList)
                {
                    if ((int)p.Tag == lid)
                    {
                        // 50 height
                        Panel ne = new Panel();
                        ne.Height = 20;
                        ne.Name = "effectpanel" + eid;
                        ne.Dock = DockStyle.Bottom;
                        ne.Padding = new Padding(0, 5, 0, 0);
                        p.Height += ne.Height;
                        ne.Tag = eid; // effect id

                        Label efn = new Label();
                        efn.Height = 15;
                        efn.Name = "effectlabel";
                        efn.Margin = Padding.Empty;
                        efn.Padding = Padding.Empty;
                        efn.Text = t.ToString();
                        efn.Dock = DockStyle.Left;

                        Button tb = new Button();
                        tb.Height = 15;
                        tb.Width = 15;
                        tb.Name = "effecthidebutton" + eid;
                        tb.BackgroundImage = Properties.Resources.eyeclose;
                        tb.BackgroundImageLayout = ImageLayout.Stretch;
                        tb.FlatStyle = FlatStyle.Flat;
                        tb.FlatAppearance.BorderSize = 0;
                        tb.Dock = DockStyle.Right;
                        tb.Click += turnEffect_Click;

                        Button ed = new Button(); // remmove effect
                        ed.Height = 15;
                        ed.Width = 15;
                        ed.BackColor = Color.Red;
                        ed.Text = "X";
                        ed.Font = new Font(ed.Font.FontFamily, 5);
                        ed.FlatStyle = FlatStyle.Flat;
                        ed.FlatAppearance.BorderSize = 0;
                        ed.Dock = DockStyle.Right;
                        ed.Click += deleteEffect_Click;

                        Button eb = new Button(); // edit effect
                        eb.Height = 15;
                        //eb.Width = 85;
                        //eb.BackColor = Color.White;
                        //eb.Text = "edit";

                        eb.Width = 20;
                        eb.BackgroundImage = Properties.Resources.tune;
                        eb.BackgroundImageLayout = ImageLayout.Stretch;
                        
                        eb.Font = new Font(eb.Font.FontFamily, 5);
                        eb.FlatStyle = FlatStyle.Flat;
                        eb.FlatAppearance.BorderSize = 0;
                        eb.Dock = DockStyle.Right;
                        eb.Click += editEffect_Click;

                        ne.Controls.Add(efn);
                        ne.Controls.Add(tb);
                        //Grayscale, Invert
                        if (t == effect_type.Grayscale || 
                            t == effect_type.Invert || 
                            t == effect_type.Grayscale2 || 
                            t == effect_type.Sepia ||
                            t == effect_type.Highlight) { } else
                        {
                            ne.Controls.Add(eb);
                        }
                        
                        ne.Controls.Add(ed);
                        p.Controls.Add(ne);
                    }
                }
            }
        }

        public void removeEffect(int lid, int eid)
        {
            if (panelList.Count > 0)
            {
                foreach (Panel p in panelList)
                {
                    if ((int)p.Tag == lid)
                    {
                        p.Controls.Remove(p.Controls.Find("effectpanel" + eid, true)[0]);
                        p.Height -= 20;
                    }
                }
            }
        }

        public void turnEffect(int lid, int eid,bool state)
        {
            if (panelList.Count > 0)
            {
                foreach (Panel p in panelList)
                {
                    if ((int)p.Tag == lid)
                    {
                        Button b = p.Controls.Find("effecthidebutton" + eid, true)[0] as Button;
                        if (state) // ON
                        {
                            b.BackgroundImage = Properties.Resources.eye;
                        }
                        else // OFF
                        {
                            b.BackgroundImage = Properties.Resources.eyeclose;
                        }
                    }
                }
            }
        }

        public void setMainProcess(MainProcess m)
        {
            mpres = m;
        }
        /*?--- EVENTS ---*/

        private void Nl_Click(object sender, EventArgs e) // layer panel clicked
        {
            if (sender.GetType() == typeof(Panel))
            {
                Panel p = sender as Panel;
                int id = (int)p.Tag;
                mpres.SelectLayer(id);
            }
            else if (sender.GetType() == typeof(PictureBox))
            {
                PictureBox pb = sender as PictureBox;
                Panel p = pb.Parent as Panel;
                int id = (int)p.Tag;
                mpres.SelectLayer(id);
            }
            else if (sender.GetType() == typeof(Label))
            {
                Label lb = sender as Label;
                Panel p = lb.Parent as Panel;
                int id = (int)p.Tag;
                mpres.SelectLayer(id);
            }

        }

        private void Hl_Click(object sender, EventArgs e) // hide layer
        {
            Button b = sender as Button;
            Panel p = b.Parent as Panel;
            int id = (int)p.Tag;
            mpres.layerHideClicked(id);
        }

        private void Layout_Click(object sender, EventArgs e)
        {
            mpres.DeselectLayer();
            deselectLayerPanel();
        }

        private void NewLayer_Click(object sender, EventArgs e)
        {
            if (form1res.IsProjectOpenned())
            {
                mpres.NewLayer();
            }
        }

        private void DeleteLayer_Click(object sender, EventArgs e)
        {
            if (form1res.IsProjectOpenned())
            {
                mpres.DeleteLayer();
            }
                
        }

        private void editEffect_Click(object sender, EventArgs e)
        {
            if (!form1res.IsProjectOpenned()) { return; }
            Button b = sender as Button;
            Panel pp = b.Parent as Panel;//eid
            int eid = (int)pp.Tag;
            Panel p = pp.Parent as Panel;//lid
            int lid = (int)p.Tag;
            Label l = pp.Controls.Find("effectlabel", true)[0] as Label;
            effect_type et = (effect_type)Enum.Parse(typeof(effect_type), l.Text);
            mpres.updateEffect(lid, eid, et);

        }

        private void deleteEffect_Click(object sender, EventArgs e)
        {
            if (!form1res.IsProjectOpenned()) { return; }
            Button b = sender as Button;
            Panel pp = b.Parent as Panel;//eid
            int eid = (int)pp.Tag;
            Panel p = pp.Parent as Panel;//lid
            int lid = (int)p.Tag;
            mpres.removeEffect(lid, eid);
        }

        private void turnEffect_Click(object sender, EventArgs e)
        {
            if (!form1res.IsProjectOpenned()) { return; }
            Button b = sender as Button;
            Panel pp = b.Parent as Panel;//eid
            int eid = (int)pp.Tag;
            Panel p = pp.Parent as Panel;//lid
            int lid = (int)p.Tag;
            mpres.turnEffect(lid, eid);
        }

        private void LayerOpacityTrackBar_valueChanged(object sender, EventArgs e)
        {
            if (!form1res.IsProjectOpenned()) { return; }
            TrackBar t = sender as TrackBar;
            mpres.updateLayerOpacity(t.Value);
        }
        private void LayerOpacity_EnabledChanged(object sender, EventArgs e)
        {
            
        }
    }
}

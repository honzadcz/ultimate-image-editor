﻿namespace PhotoEditorV2
{
    partial class Form1
    {
        /// <summary>
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód generovaný Návrhářem Windows Form

        /// <summary>
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.filtersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addImageToLayerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.meToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.toolbarlayout = new System.Windows.Forms.FlowLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.windowtoolbarlayout = new System.Windows.Forms.FlowLayoutPanel();
            this.toolbarstrippanel = new System.Windows.Forms.Panel();
            this.Form1Layout = new System.Windows.Forms.TableLayoutPanel();
            this.Form1ButtonPanel = new System.Windows.Forms.Panel();
            this.ProjectNameLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Form1ButtonPanelLayout = new System.Windows.Forms.FlowLayoutPanel();
            this.CloseFormButton = new System.Windows.Forms.Button();
            this.MaximalizeFormButton = new System.Windows.Forms.Button();
            this.MinimalizeFormButton = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.Form1Layout.SuspendLayout();
            this.Form1ButtonPanel.SuspendLayout();
            this.Form1ButtonPanelLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.menuStrip1.GripMargin = new System.Windows.Forms.Padding(0);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.imageToolStripMenuItem,
            this.editToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 25);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(0);
            this.menuStrip1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuStrip1.Size = new System.Drawing.Size(984, 20);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.saveToolStripMenuItem});
            this.fileToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.BackColor = System.Drawing.Color.Gray;
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.BackColor = System.Drawing.Color.Gray;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // imageToolStripMenuItem
            // 
            this.imageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importToolStripMenuItem,
            this.filtersToolStripMenuItem});
            this.imageToolStripMenuItem.Name = "imageToolStripMenuItem";
            this.imageToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.imageToolStripMenuItem.Text = "Image";
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.BackColor = System.Drawing.Color.Gray;
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.importToolStripMenuItem.Text = "Import";
            this.importToolStripMenuItem.Click += new System.EventHandler(this.importToolStripMenuItem_Click);
            // 
            // filtersToolStripMenuItem
            // 
            this.filtersToolStripMenuItem.BackColor = System.Drawing.Color.Gray;
            this.filtersToolStripMenuItem.Name = "filtersToolStripMenuItem";
            this.filtersToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.filtersToolStripMenuItem.Text = "Filters";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addImageToLayerToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // addImageToLayerToolStripMenuItem
            // 
            this.addImageToLayerToolStripMenuItem.BackColor = System.Drawing.Color.Gray;
            this.addImageToLayerToolStripMenuItem.Name = "addImageToLayerToolStripMenuItem";
            this.addImageToLayerToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.addImageToLayerToolStripMenuItem.Text = "Add image to layer";
            this.addImageToLayerToolStripMenuItem.Click += new System.EventHandler(this.addImageToLayerToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.meToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // meToolStripMenuItem
            // 
            this.meToolStripMenuItem.BackColor = System.Drawing.Color.Maroon;
            this.meToolStripMenuItem.Name = "meToolStripMenuItem";
            this.meToolStripMenuItem.Size = new System.Drawing.Size(91, 22);
            this.meToolStripMenuItem.Text = "Me";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.toolbarlayout, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.windowtoolbarlayout, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 95);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(984, 600);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // toolbarlayout
            // 
            this.toolbarlayout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(165)))), ((int)(((byte)(166)))));
            this.toolbarlayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolbarlayout.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.toolbarlayout.Location = new System.Drawing.Point(0, 0);
            this.toolbarlayout.Margin = new System.Windows.Forms.Padding(0);
            this.toolbarlayout.MaximumSize = new System.Drawing.Size(40, 0);
            this.toolbarlayout.Name = "toolbarlayout";
            this.toolbarlayout.Size = new System.Drawing.Size(40, 560);
            this.toolbarlayout.TabIndex = 3;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(140)))), ((int)(((byte)(141)))));
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Enabled = false;
            this.pictureBox1.Location = new System.Drawing.Point(40, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(708, 560);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // windowtoolbarlayout
            // 
            this.windowtoolbarlayout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(165)))), ((int)(((byte)(166)))));
            this.windowtoolbarlayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.windowtoolbarlayout.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.windowtoolbarlayout.Location = new System.Drawing.Point(748, 0);
            this.windowtoolbarlayout.Margin = new System.Windows.Forms.Padding(0);
            this.windowtoolbarlayout.Name = "windowtoolbarlayout";
            this.windowtoolbarlayout.Size = new System.Drawing.Size(236, 560);
            this.windowtoolbarlayout.TabIndex = 5;
            // 
            // toolbarstrippanel
            // 
            this.toolbarstrippanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(93)))), ((int)(((byte)(119)))));
            this.toolbarstrippanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolbarstrippanel.Location = new System.Drawing.Point(0, 45);
            this.toolbarstrippanel.Margin = new System.Windows.Forms.Padding(0);
            this.toolbarstrippanel.Name = "toolbarstrippanel";
            this.toolbarstrippanel.Size = new System.Drawing.Size(984, 50);
            this.toolbarstrippanel.TabIndex = 6;
            // 
            // Form1Layout
            // 
            this.Form1Layout.ColumnCount = 1;
            this.Form1Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.Form1Layout.Controls.Add(this.Form1ButtonPanel, 0, 0);
            this.Form1Layout.Controls.Add(this.toolbarstrippanel, 0, 2);
            this.Form1Layout.Controls.Add(this.menuStrip1, 0, 1);
            this.Form1Layout.Controls.Add(this.tableLayoutPanel1, 0, 3);
            this.Form1Layout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Form1Layout.Location = new System.Drawing.Point(0, 0);
            this.Form1Layout.Margin = new System.Windows.Forms.Padding(0);
            this.Form1Layout.Name = "Form1Layout";
            this.Form1Layout.RowCount = 4;
            this.Form1Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.Form1Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.Form1Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.Form1Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.Form1Layout.Size = new System.Drawing.Size(984, 650);
            this.Form1Layout.TabIndex = 2;
            // 
            // Form1ButtonPanel
            // 
            this.Form1ButtonPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(62)))), ((int)(((byte)(80)))));
            this.Form1ButtonPanel.Controls.Add(this.ProjectNameLabel);
            this.Form1ButtonPanel.Controls.Add(this.panel1);
            this.Form1ButtonPanel.Controls.Add(this.Form1ButtonPanelLayout);
            this.Form1ButtonPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Form1ButtonPanel.Location = new System.Drawing.Point(0, 0);
            this.Form1ButtonPanel.Margin = new System.Windows.Forms.Padding(0);
            this.Form1ButtonPanel.Name = "Form1ButtonPanel";
            this.Form1ButtonPanel.Size = new System.Drawing.Size(984, 25);
            this.Form1ButtonPanel.TabIndex = 2;
            this.Form1ButtonPanel.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Form1ButtonPanel_MouseDoubleClick);
            this.Form1ButtonPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1ButtonPanel_MouseDown);
            this.Form1ButtonPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1ButtonPanel_MouseMove);
            this.Form1ButtonPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1ButtonPanel_MouseUp);
            // 
            // ProjectNameLabel
            // 
            this.ProjectNameLabel.AutoSize = true;
            this.ProjectNameLabel.Enabled = false;
            this.ProjectNameLabel.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.ProjectNameLabel.Location = new System.Drawing.Point(43, 6);
            this.ProjectNameLabel.Margin = new System.Windows.Forms.Padding(0);
            this.ProjectNameLabel.Name = "ProjectNameLabel";
            this.ProjectNameLabel.Size = new System.Drawing.Size(100, 13);
            this.ProjectNameLabel.TabIndex = 2;
            this.ProjectNameLabel.Text = "Untitled() - Project 1";
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::PhotoEditorV2.Properties.Resources.programnameimage;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(40, 25);
            this.panel1.TabIndex = 1;
            // 
            // Form1ButtonPanelLayout
            // 
            this.Form1ButtonPanelLayout.Controls.Add(this.CloseFormButton);
            this.Form1ButtonPanelLayout.Controls.Add(this.MaximalizeFormButton);
            this.Form1ButtonPanelLayout.Controls.Add(this.MinimalizeFormButton);
            this.Form1ButtonPanelLayout.Dock = System.Windows.Forms.DockStyle.Right;
            this.Form1ButtonPanelLayout.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.Form1ButtonPanelLayout.Location = new System.Drawing.Point(878, 0);
            this.Form1ButtonPanelLayout.Name = "Form1ButtonPanelLayout";
            this.Form1ButtonPanelLayout.Size = new System.Drawing.Size(106, 25);
            this.Form1ButtonPanelLayout.TabIndex = 0;
            // 
            // CloseFormButton
            // 
            this.CloseFormButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CloseFormButton.BackgroundImage")));
            this.CloseFormButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.CloseFormButton.FlatAppearance.BorderSize = 0;
            this.CloseFormButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CloseFormButton.Location = new System.Drawing.Point(71, 0);
            this.CloseFormButton.Margin = new System.Windows.Forms.Padding(0);
            this.CloseFormButton.Name = "CloseFormButton";
            this.CloseFormButton.Size = new System.Drawing.Size(35, 25);
            this.CloseFormButton.TabIndex = 0;
            this.CloseFormButton.UseVisualStyleBackColor = true;
            this.CloseFormButton.Click += new System.EventHandler(this.CloseFormButton_Click);
            // 
            // MaximalizeFormButton
            // 
            this.MaximalizeFormButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("MaximalizeFormButton.BackgroundImage")));
            this.MaximalizeFormButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.MaximalizeFormButton.FlatAppearance.BorderSize = 0;
            this.MaximalizeFormButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MaximalizeFormButton.Location = new System.Drawing.Point(36, 0);
            this.MaximalizeFormButton.Margin = new System.Windows.Forms.Padding(0);
            this.MaximalizeFormButton.Name = "MaximalizeFormButton";
            this.MaximalizeFormButton.Size = new System.Drawing.Size(35, 25);
            this.MaximalizeFormButton.TabIndex = 1;
            this.MaximalizeFormButton.UseVisualStyleBackColor = true;
            this.MaximalizeFormButton.Click += new System.EventHandler(this.MaximalizeFormButton_Click);
            // 
            // MinimalizeFormButton
            // 
            this.MinimalizeFormButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("MinimalizeFormButton.BackgroundImage")));
            this.MinimalizeFormButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.MinimalizeFormButton.FlatAppearance.BorderSize = 0;
            this.MinimalizeFormButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MinimalizeFormButton.Location = new System.Drawing.Point(1, 0);
            this.MinimalizeFormButton.Margin = new System.Windows.Forms.Padding(0);
            this.MinimalizeFormButton.Name = "MinimalizeFormButton";
            this.MinimalizeFormButton.Size = new System.Drawing.Size(35, 25);
            this.MinimalizeFormButton.TabIndex = 2;
            this.MinimalizeFormButton.UseVisualStyleBackColor = true;
            this.MinimalizeFormButton.Click += new System.EventHandler(this.MinimalizeFormButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GrayText;
            this.ClientSize = new System.Drawing.Size(984, 650);
            this.Controls.Add(this.Form1Layout);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(750, 650);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Phoutoushoup";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.Form1Layout.ResumeLayout(false);
            this.Form1Layout.PerformLayout();
            this.Form1ButtonPanel.ResumeLayout(false);
            this.Form1ButtonPanel.PerformLayout();
            this.Form1ButtonPanelLayout.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel toolbarlayout;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem filtersToolStripMenuItem;
        private System.Windows.Forms.FlowLayoutPanel windowtoolbarlayout;
        private System.Windows.Forms.ToolStripMenuItem meToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addImageToLayerToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel toolbarstrippanel;
        private System.Windows.Forms.TableLayoutPanel Form1Layout;
        private System.Windows.Forms.Panel Form1ButtonPanel;
        private System.Windows.Forms.FlowLayoutPanel Form1ButtonPanelLayout;
        private System.Windows.Forms.Button CloseFormButton;
        private System.Windows.Forms.Button MaximalizeFormButton;
        private System.Windows.Forms.Button MinimalizeFormButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label ProjectNameLabel;
    }
}


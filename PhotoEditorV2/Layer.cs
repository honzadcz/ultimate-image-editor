﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoEditorV2
{
    class Layer
    {
        /*?REFERENCE*/
        private Form1 formref;
        private MainProcess mpref;
        private WindowToolbar wtbref;

        /*?LAYER*/
        private int layerId;
        private bool layerEnabled = true;
        private string layerName;
        private int layerOpacity = 100;
        private Bitmap original; // original + drawings
        private Bitmap rendered; // with filters
        
        private Rectangle maxSize;
        private List<Effect> effects;
        private int effectid = 0;

        private Point layerMoved;

        

        public Layer(Form1 form, MainProcess mp, WindowToolbar wtb)
        {
            this.formref = form;
            this.mpref = mp;
            this.wtbref = wtb;
            layerId = mpref.GetNextLayerId();
            init();
        }

        ~Layer()
        {
            if(original != null)
            original.Dispose();
            if(rendered != null)
            rendered.Dispose();
            GC.Collect();
        }

        private void init()
        {
            effects = new List<Effect>();
            layerMoved = new Point(0, 0);
            maxSize = formref.GetProjectSize();
            original = null/*new Bitmap(maxSize.Width, maxSize.Height)*/;
            wtbref.createNewLayerPanel(layerId);
            SelectLayer();
        }

        private void Render()
        {
            if(original == null) { return; }

            Bitmap nr = (Bitmap)original.Clone();

            if(effects.Count > 0)
            {
                for(int i = 0; i < effects.Count; i++)
                {
                    Effect e = effects[i];
                    if (e.Effect_on)
                    {
                        nr = e.Process(nr);
                    }
                }
            }
            rendered = (Bitmap)nr.Clone();
            wtbref.updateLayerThumbnail(layerId,(Bitmap)nr.Clone());
            nr.Dispose();
            GC.Collect();
        }

        public void AddEffect(effect_type t)
        {
            effects.Add(new Effect(this, t,effectid));
            wtbref.addNewEffect(layerId,effectid,t);
            effectid++;
            Render();
        }

        public void RemoveEffect(int id) //! get effect in list by index
        {
            foreach(Effect e in effects)
            {
                if(e.Id == id)
                {
                    effects.Remove(e);
                    wtbref.removeEffect(layerId, id);
                    break;
                }
            }
            Render();
        }

        public void UpdateEffec(int id,object values) //! get effect in list by index
        {
            foreach(Effect e in effects)
            {
                if(e.Id == id)
                {
                    switch (e.CurrentEffect)
                    {
                        case effect_type.ColorFilter:
                            FilterValue_ColorFilter cf = values as FilterValue_ColorFilter;
                            e.RgbFilter_r = cf.rgbFilter_r;
                            e.RgbFilter_g = cf.rgbFilter_g;
                            e.RgbFilter_b = cf.rgbFilter_b;
                            break;
                        case effect_type.Gamma:
                            FilterValue_Gamma gm = values as FilterValue_Gamma;
                            e.Gamma_r = gm.gamma_r;
                            e.Gamma_g = gm.gamma_g;
                            e.Gamma_b = gm.gamma_b;
                            break;
                        case effect_type.Brightness:
                            FilterValue_Brightness bs = values as FilterValue_Brightness;
                            e.Brightness_value = bs.brightness_value;
                            break;
                        case effect_type.Contrast:
                            FilterValue_Contrast ct = values as FilterValue_Contrast;
                            e.Contrast_value = ct.contrast_value;
                            break;
                    }

                    break;
                }
            }
            Render();
        }

        public void TurnEffect(int id)
        {
            foreach(Effect e in effects)
            {
                if(e.Id == id)
                {
                    e.Effect_on = !e.Effect_on;
                    wtbref.turnEffect(layerId, id, e.Effect_on);
                    break;
                }
            }
            Render();
        }

        public void AddPicture(Image np) 
        {
            //Image npc = np.Clone() as Image;
            Bitmap npc = (Bitmap)np;
            if (original == null)
            {
                original = (Bitmap)npc.Clone();
                Console.WriteLine("Image AddPicture from Layer created new picture");
            }
            else
            {
                Color p = new Color();
                for (int x = 0; x < original.Width; x++)
                {
                    if (x >= npc.Width) { continue; }
                    for (int y = 0; y < original.Height; y++)
                    {
                        if (y >= npc.Height) { continue; }
                        p = npc.GetPixel(x, y);
                        original.SetPixel(x, y, Color.FromArgb(LayerOpacity, p));
                    }
                }
                Console.WriteLine("Image AddPicture from Layer added to image");
            }

            Render();
            np.Dispose();
            npc.Dispose();
            GC.Collect();

        }

        public void Erase(int x, int y) //! not working idk
        {
            if (original == null) { return; }
            int bs = mpref.Erase_size;
            Rectangle r = new Rectangle(x, y, bs, bs);
            using (Graphics g = Graphics.FromImage(original))
            {
                using (Pen p = new Pen(Color.Transparent))
                {
                    g.FillRectangle(p.Brush, r);
                    g.Save();
                    g.Dispose();
                }
            }






            //! delete erase original
            //! if mouse pressed


        }

        public void Paint(int x, int y) 
        {
            int bs = mpref.Brush_size;
            Rectangle r = new Rectangle(x, y, bs, bs);

            if (original == null)
            {
                original = new Bitmap(maxSize.Width, maxSize.Height);
                using(Graphics g = Graphics.FromImage(original))
                {
                    using (Brush b = Brushes.Transparent)
                    {
                        g.FillRectangle(b,r);
                        g.Save();
                        g.Dispose();
                    }
                }
            }
            //! paint on original
            //! if mouse pressed
            //! RESIZE    if(x > original.Width) { original.Size = new Size(x, original.Height); } 
            
            using (Graphics g = Graphics.FromImage(original))
            {
                using(Pen p = new Pen(mpref.Brush_color))
                {
                    g.FillRectangle(p.Brush, r);
                    g.Save();
                    g.Dispose();
                }
            }
            Render();
        }

        public void moveLayer(int x, int y)
        {
            Point start = formref.GetLastClickPosition();
            layerMoved = new Point(x - start.X, y - start.Y);
            Render();
        }

        public void SelectLayer() // select this layer
        {
            wtbref.setSelectedLayerPanel(layerId);
        }

        public void OnDelete()
        {
            if (original != null)
                original.Dispose();
            if (rendered != null)
                rendered.Dispose();

            GC.Collect();
        }

        public bool IsEmpty()
        {
            if(original == null || rendered == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int LayerId { get => layerId; }
        public string LayerName { get => layerName; set => layerName = value; }
        public Bitmap Rendered { get => rendered;}
        public int LayerOpacity { get => layerOpacity; set => layerOpacity = value; }
        public bool LayerEnabled { get => layerEnabled; set => layerEnabled = value; }
        public Point LayerMoved { get => layerMoved; set => layerMoved = value; }

        public Effect GetEffectById(int id)
        {
            Effect ret = null;
            foreach (Effect e in effects)
            {
                if (e.Id == id)
                {
                    ret = e;
                    break;
                }
            }
            return ret;
        }
    }
    public class FilterValue_ColorFilter
    {
        public int rgbFilter_r;
        public int rgbFilter_g;
        public int rgbFilter_b;
        public FilterValue_ColorFilter(int r, int g, int b)
        {
            rgbFilter_r = r;
            rgbFilter_g = g;
            rgbFilter_b = b;
        }
    }
    public class FilterValue_Gamma
    {
        public float gamma_r;
        public float gamma_g;
        public float gamma_b;
        public FilterValue_Gamma(float r, float g, float b)
        {
            gamma_r = r;
            gamma_g = g;
            gamma_b = b;
        }
    }
    public class FilterValue_Brightness
    {
        public int brightness_value;
        public FilterValue_Brightness(int v)
        {
            brightness_value = v;
        }
    }
    public class FilterValue_Contrast
    {
        public int contrast_value;
        public FilterValue_Contrast(int v)
        {
            contrast_value = v;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoEditorV2
{
    public enum effect_type { ColorFilter ,Gamma , Brightness, Contrast, Grayscale, Invert, Grayscale2, Sepia, Highlight };

    class Effect
    {
        /*?REFERENCE*/
        private Layer lref;

        /*?EFFECT*/
        private effect_type cet;
        private int id;

        private bool effect_on = false;

        /*?--- ColorFilter ---*/
        private int rgbFilter_r = 0;
        private int rgbFilter_g = 0;
        private int rgbFilter_b = 0;
        /*?--- Gamma ---*/
        private float gamma_r = 2.3f;
        private float gamma_g = 2.3f;
        private float gamma_b = 2.3f;
        /*?--- Brightness ---*/
        private int brightness_value = 0;
        /*?--- Contrast ---*/
        private int contrast_value = 0;

        

        public Effect(Layer l, effect_type et, int id)
        {
            this.lref = l;
            this.cet = et;
            this.id = id;
        }

        public Bitmap Process(Bitmap b)
        {
            Bitmap ret = null;
            Bitmap bc = (Bitmap)b.Clone();
            Color c;
            switch (cet)
            {
                case effect_type.ColorFilter:
                    for (int i = 0; i < bc.Width; i++)
                    {
                        for (int j = 0; j < bc.Height; j++)
                        {
                            c = bc.GetPixel(i, j);

                            int nr = c.R + rgbFilter_r;
                            nr = Math.Max(nr, 0);
                            nr = Math.Min(255, nr);

                            int ng = c.G + rgbFilter_g;
                            ng = Math.Max(ng, 0);
                            ng = Math.Min(255, ng);

                            int nb = c.B + rgbFilter_b;
                            nb = Math.Max(nb, 0);
                            nb = Math.Min(255, nb);

                            bc.SetPixel(i, j, Color.FromArgb(c.A,nr, ng, nb));
                        }
                    }
                    ret = (Bitmap)bc.Clone();
                    break;
                case effect_type.Gamma:
                    byte[] redGamma = CreateGammaArray(gamma_r);
                    byte[] greenGamma = CreateGammaArray(gamma_g);
                    byte[] blueGamma = CreateGammaArray(gamma_b);
                    for (int i = 0; i < bc.Width; i++)
                    {
                        for (int j = 0; j < bc.Height; j++)
                        {
                            c = bc.GetPixel(i, j);
                            bc.SetPixel(i, j, Color.FromArgb(c.A,redGamma[c.R], greenGamma[c.G], blueGamma[c.B]));
                        }
                    }
                    ret = (Bitmap)bc.Clone();
                    break;
                case effect_type.Brightness:
                    for (int i = 0; i < bc.Width; i++)
                    {
                        for (int j = 0; j < bc.Height; j++)
                        {
                            c = bc.GetPixel(i, j);
                            int br = c.R + brightness_value;
                            int bg = c.G + brightness_value;
                            int bb = c.B + brightness_value;

                            if (br < 0) { br = 1; }
                            else if (br > 255) { br = 255; }

                            if (bg < 0) { bg = 1; }
                            else if (bg > 255) { bg = 255; }

                            if (bb < 0) { bb = 1; }
                            else if (bb > 255) { bb = 255; }
                            bc.SetPixel(i, j, Color.FromArgb(c.A,br, bg, bb));
                        }
                    }
                    ret = (Bitmap)bc.Clone();
                    break;
                case effect_type.Contrast:
                    for (int i = 0; i < bc.Width; i++)
                    {
                        for (int j = 0; j < bc.Height; j++)
                        {
                            c = bc.GetPixel(i, j);
                            double cr = ((((c.R / 255.0) - 0.5) * contrast_value) + 0.5) * 255.0;
                            if (cr < 0) { cr = 0; }
                            if (cr > 255) { cr = 255; }

                            double cg = ((((c.G / 255.0) - 0.5) * contrast_value) + 0.5) * 255.0;
                            if (cg < 0) { cg = 0; }
                            if (cg > 255) { cg = 255; }

                            double cb = ((((c.B / 255.0) - 0.5) * contrast_value) + 0.5) * 255.0;
                            if (cb < 0) { cb = 0; }
                            if (cb > 255) { cb = 255; }

                            bc.SetPixel(i, j, Color.FromArgb(c.A,(byte)cr, (byte)cg, (byte)cb));
                        }
                    }
                    ret = (Bitmap)bc.Clone();
                    break;
                case effect_type.Grayscale:
                    for (int i = 0; i < bc.Width; i++)
                    {
                        for (int j = 0; j < bc.Height; j++)
                        {
                            c = bc.GetPixel(i, j);
                            int p = (c.R + c.G + c.B) / 3;
                            bc.SetPixel(i, j, Color.FromArgb(c.A,(byte)p, (byte)p, (byte)p));
                        }
                    }
                    ret = (Bitmap)bc.Clone();
                    break;
                case effect_type.Invert:
                    for (int i = 0; i < bc.Width; i++)
                    {
                        for (int j = 0; j < bc.Height; j++)
                        {
                            c = bc.GetPixel(i, j);
                            bc.SetPixel(i, j, Color.FromArgb(c.A,255 - c.R, 255 - c.G, 255 - c.B));
                        }
                    }
                    ret = (Bitmap)bc.Clone();
                    break;
                case effect_type.Grayscale2:

                    for (int i = 0; i < bc.Width; i++)
                    {
                        for (int j = 0; j < bc.Height; j++)
                        {
                            c = bc.GetPixel(i, j);
                            /*int rr = 0;
                            int gr = 0;
                            int br = 0;
                            
                            if(c.R > 137) { rr = 255; } else { rr = 0; }
                            if(c.G > 137) { gr = 255; } else { gr = 0; }
                            if(c.G > 137) { br = 255; } else { br = 0; }
                            bc.SetPixel(i, j, Color.FromArgb(c.A,rr, gr, br));*/
                            int p = (c.R + c.G + c.B) / 3;
                            if(p > 137) { p = 255; } else { p = 0; }
                            bc.SetPixel(i, j, Color.FromArgb(c.A, (byte)p, (byte)p, (byte)p));
                        }
                    }
                    ret = (Bitmap)bc.Clone();

                    break;
                case effect_type.Sepia:
                    for (int i = 0; i < bc.Width; i++)
                    {
                        for (int j = 0; j < bc.Height; j++)
                        {
                            c = bc.GetPixel(i, j);
                            int sa = c.A;
                            int sr = c.R;
                            int sg = c.G;
                            int sb = c.B;

                            int tr = (int)(0.393 * sr + 0.769 * sg + 0.189 * sb);
                            int tg = (int)(0.349 * sr + 0.686 * sg + 0.168 * sb);
                            int tb = (int)(0.272 * sr + 0.534 * sg + 0.131 * sb);
                            if(tr > 255) { sr = 255; } else { sr = tr; }
                            if(tg > 255) { sg = 255; } else { sg = tg; }
                            if(tb > 255) { sb = 255; } else { sb = tb; }
                            bc.SetPixel(i, j, Color.FromArgb(sa,sr, sg ,sb));
                        }
                    }
                    ret = (Bitmap)bc.Clone();
                    break;
                case effect_type.Highlight:
                    for (int i = 0; i < bc.Width; i++)
                    {
                        for (int j = 0; j < bc.Height; j++)
                        {
                            c = bc.GetPixel(i, j);
                            int rr = 0;
                            int gr = 0;
                            int br = 0;
                            
                            if(c.R > 137) { rr = 255; } else { rr = 0; }
                            if(c.G > 137) { gr = 255; } else { gr = 0; }
                            if(c.G > 137) { br = 255; } else { br = 0; }
                            bc.SetPixel(i, j, Color.FromArgb(c.A,rr, gr, br));
                        }
                    }
                    ret = (Bitmap)bc.Clone();
                    break;
            }

            bc.Dispose();
            b.Dispose();
            GC.Collect();

            return ret;
        }

        private byte[] CreateGammaArray(double color)
        {
            byte[] gammaArray = new byte[256];
            for (int i = 0; i < 256; ++i)
            {
                gammaArray[i] = (byte)Math.Min(255, (int)((255.0 * Math.Pow(i / 255.0, 1.0 / color)) + 0.5));
            }
            return gammaArray;
        }

        

        public int Id { get => id; set => id = value; }

        public effect_type CurrentEffect { get => cet; set => cet = value; }

        public bool Effect_on { get => effect_on; set => effect_on = value; }

        public int RgbFilter_r { get => rgbFilter_r; set => rgbFilter_r = value; }
        public int RgbFilter_g { get => rgbFilter_g; set => rgbFilter_g = value; }
        public int RgbFilter_b { get => rgbFilter_b; set => rgbFilter_b = value; }

        public float Gamma_r { get => gamma_r; set => gamma_r = value; }
        public float Gamma_g { get => gamma_g; set => gamma_g = value; }
        public float Gamma_b { get => gamma_b; set => gamma_b = value; }

        public int Brightness_value { get => brightness_value; set => brightness_value = value; }

        public int Contrast_value { get => contrast_value; set => contrast_value = value; }
        
    }
}

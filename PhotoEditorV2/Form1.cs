﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace PhotoEditorV2
{
    public enum tool_type { Move, Paint, Erase, Type };

    public partial class Form1 : Form
    {
        private ToolbarPanel tbp;
        private WindowToolbar wtb;
        private MainProcess mp;

        private bool FormBeingDragged = false;
        private int FormMouseX;
        private int FormMouseY;

        private tool_type selectedTool;
        private bool isMousePressed = false;

        private bool isProjectOpenned = false;
        private int projectNumber = 0;
        private string projectName = "";
        private Rectangle projectSize;
        private string programNameString = "Ultimate image editor";
        private Point lastMouseClick = new Point(0,0);

        public Form1()
        {
            InitializeComponent();
            tbp = new ToolbarPanel(this, toolbarlayout, toolbarstrippanel);
            wtb = new WindowToolbar(this, windowtoolbarlayout);
            mp  = new MainProcess(this, pictureBox1,wtb);
            wtb.setMainProcess(mp);
            tbp.SetMainProcess(mp);
            menuStrip1.Renderer = new MyRenderer();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            init();
            
        }
        
        private void init()
        {
            setupFilterMenu();
            toolbarlayout.BackColor = Color.FromArgb(149, 165, 166);
            windowtoolbarlayout.BackColor = Color.FromArgb(149, 165, 166);
            ProjectNameLabel.Text = programNameString + " - No Project";
            pictureBox1.BackColor = Color.FromArgb(127, 140, 141);
            pictureBox1.Enabled = false;
            ProjectNameLabel.ForeColor = Color.White;
            
        }

        private void setupFilterMenu()
        {
            var values = Enum.GetValues(typeof(effect_type));
            foreach(effect_type e in values)
            {
                ToolStripItem b = new ToolStripMenuItem(e.ToString());
                b.BackColor = Color.Gray;
                b.Tag = e.ToString();
                b.ForeColor = Color.White;
                b.Click += addFilter_Click;
                filtersToolStripMenuItem.DropDownItems.Add(b);
            }
            foreach(ToolStripItem i in menuStrip1.Items)
            {
                i.ForeColor = Color.White;

            }
         
        }

        

        private void newProject(string pn, int w, int h, bool t)
        {
            pictureBox1.Enabled = true;
            isProjectOpenned = true;
            projectSize = new Rectangle(0, 0, w, h);

            wtb.newProject();
            mp.newProject();
            

            Bitmap b;
            if (!t) 
            {
                b = new Bitmap(w, h);
                using(Graphics g = Graphics.FromImage(b))
                {
                    using(Pen p = new Pen(Brushes.White))
                    {
                        g.FillRectangle(p.Brush, projectSize);
                        g.Save();
                        g.Dispose();
                    }
                }
            }
            else
            {
                b = new Bitmap(w, h);
            }

            //Console.WriteLine("Image mp.AddImage() from Form1 NewProject ");
            mp.AddImage((Image)b.Clone(), true);

            ProjectNameLabel.Text = programNameString + " - " + pn;
            projectName = pn;
            projectNumber++;
            
            
            b.Dispose();
            GC.Collect();
        }

        public void setSelectedTool(tool_type t)
        {
            selectedTool = t;
            updateTool();
        }
        private void updateTool()
        {
            switch (selectedTool)
            {
                case tool_type.Move:
                    pictureBox1.Cursor = System.Windows.Forms.Cursors.Default;
                    break;
                case tool_type.Paint:
                    pictureBox1.Cursor = new Cursor(Properties.Resources.brush.GetHicon());
                    break;
                case tool_type.Erase:
                    pictureBox1.Cursor = new Cursor(Properties.Resources.erase.GetHicon());
                    break;
                case tool_type.Type:
                    pictureBox1.Cursor = new Cursor(Properties.Resources.font.GetHicon());
                    break;
            }
        }


        private void openNewProjectForm()
        {
            Form np = new Form();
            np.Width = 300;
            np.Height = 320;
            np.MaximizeBox = false;
            np.MinimizeBox = false;
            np.FormBorderStyle = FormBorderStyle.FixedSingle;
            np.StartPosition = FormStartPosition.CenterScreen;
            np.Text = "New project";

            FlowLayoutPanel flp = new FlowLayoutPanel();
            flp.Dock = DockStyle.Fill;
            flp.FlowDirection = FlowDirection.TopDown;
            np.Controls.Add(flp);

            Panel p1 = new Panel();
            p1.Height = 50;
            p1.Width = flp.Width;
            p1.Name = "p1";
            FlowLayoutPanel p1flp = new FlowLayoutPanel();
            p1flp.Dock = DockStyle.Fill;
            p1flp.FlowDirection = FlowDirection.LeftToRight;
            p1.Controls.Add(p1flp);
            Label p1label = new Label();
            p1label.Text = "Name: ";
            p1flp.Controls.Add(p1label);
            TextBox p1pn = new TextBox();
            p1pn.Height = p1flp.Height;
            p1pn.Width = 100;
            p1pn.Name = "textboxname";
            p1pn.Text = "Untitled(" + projectNumber + ")";
            p1flp.Controls.Add(p1pn);
            flp.Controls.Add(p1);

            Panel p2 = new Panel();
            p2.Height = 50;
            p2.Width = flp.Width;
            p2.Name = "p2";
            FlowLayoutPanel p2flp = new FlowLayoutPanel();
            p2flp.Dock = DockStyle.Fill;
            p2flp.FlowDirection = FlowDirection.LeftToRight;
            p2.Controls.Add(p2flp);
            Label p2label = new Label();
            p2label.Text = "Width: ";
            p2flp.Controls.Add(p2label);
            TextBox p2wh = new TextBox();
            p2wh.Height = p2flp.Height;
            p2wh.Width = 100;
            p2wh.Name = "textboxwidth";
            p2wh.Text = "500";
            p2flp.Controls.Add(p2wh);
            flp.Controls.Add(p2);

            Panel p3 = new Panel();
            p3.Height = 50;
            p3.Width = flp.Width;
            p3.Name = "p3";
            FlowLayoutPanel p3flp = new FlowLayoutPanel();
            p3flp.Dock = DockStyle.Fill;
            p3flp.FlowDirection = FlowDirection.LeftToRight;
            p3.Controls.Add(p3flp);
            Label p3label = new Label();
            p3label.Text = "Height: ";
            p3flp.Controls.Add(p3label);
            TextBox p3hh = new TextBox();
            p3hh.Height = p3flp.Height;
            p3hh.Width = 100;
            p3hh.Name = "textboxheight";
            p3hh.Text = "500";
            p3flp.Controls.Add(p3hh);
            flp.Controls.Add(p3);

            Panel p4 = new Panel();
            p4.Height = 50;
            p4.Width = flp.Width;
            p4.Name = "p4";
            FlowLayoutPanel p4flp = new FlowLayoutPanel();
            p4flp.Dock = DockStyle.Fill;
            p4flp.FlowDirection = FlowDirection.LeftToRight;
            p4.Controls.Add(p4flp);
            CheckBox p4t = new CheckBox();
            p4t.Height = 30;
            p4t.Width = 100;
            p4t.Name = "checkboxtransparent";
            p4t.Text = "Transparent background";
            p4flp.Controls.Add(p4t);
            flp.Controls.Add(p4);

            Panel p5 = new Panel();
            p5.Height = 50;
            p5.Width = flp.Width;
            p5.Name = "p5";
            FlowLayoutPanel p5flp = new FlowLayoutPanel();
            p5flp.Dock = DockStyle.Fill;
            p5flp.FlowDirection = FlowDirection.LeftToRight;
            p5.Controls.Add(p5flp);
            Button p5ok = new Button();
            p5ok.Height = 30;
            p5ok.Width = 100;
            p5ok.Text = "OK";
            p5ok.Click += P5ok_Click;
            p5flp.Controls.Add(p5ok);
            flp.Controls.Add(p5);
            np.ShowDialog();
        }

        private void P5ok_Click(object sender, EventArgs e)
        {
            Button b = sender as Button;
            Form parentForm = b.Parent.Parent.Parent.Parent as Form;
            FlowLayoutPanel parentLayout = b.Parent.Parent.Parent as FlowLayoutPanel;
            TextBox  pn = parentLayout.Controls.Find("textboxname", true)[0] as TextBox;
            TextBox  pw = parentLayout.Controls.Find("textboxwidth", true)[0] as TextBox;
            TextBox  ph = parentLayout.Controls.Find("textboxheight", true)[0] as TextBox;
            CheckBox pt = parentLayout.Controls.Find("checkboxtransparent", true)[0] as CheckBox;

            string projectName = pn.Text;
            int width = int.Parse(pw.Text);
            int height = int.Parse(ph.Text);
            bool transparent = pt.Checked;
            

            newProject(projectName, width, height, transparent);
            parentForm.Close();
        }

        public bool IsProjectOpenned()
        {
            return isProjectOpenned;
        }
        public tool_type CurrentToolType()
        {
            return selectedTool;
        }
        public Rectangle GetProjectSize()
        {
            return projectSize ;
        }
        public Point GetLastClickPosition()
        {
            return lastMouseClick;
        }
        

        /*?--- EVENTS ---*/

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (isProjectOpenned)
            {
                DialogResult dr = MessageBox.Show("Do you want to close this project without saving?", "Create new project", MessageBoxButtons.YesNo);
                if(dr == DialogResult.Yes)
                {
                    openNewProjectForm();
                }
            }
            else
            {
                openNewProjectForm();
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e) // save image
        {
            if (!isProjectOpenned) { return; }
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.InitialDirectory = "c:\\";
            sfd.Filter = "JPeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif";
            sfd.Title = "Export an Image File";
            sfd.FileName = projectName;
            sfd.RestoreDirectory = true;
            if(sfd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if(sfd.FileName != null || sfd.FileName != "")
                    {
                        FileStream fs = (FileStream)sfd.OpenFile();
                        switch (sfd.FilterIndex)
                        {
                            case 1:
                                //! change to the GetPicture() instead of this.pictureBox1
                                this.pictureBox1.Image.Save(fs,
                                   System.Drawing.Imaging.ImageFormat.Jpeg);
                                break;

                            case 2:
                                //! change to the GetPicture() instead of this.pictureBox1
                                this.pictureBox1.Image.Save(fs,
                                   System.Drawing.Imaging.ImageFormat.Bmp);
                                break;

                            case 3:
                                //! change to the GetPicture() instead of this.pictureBox1
                                this.pictureBox1.Image.Save(fs,
                                   System.Drawing.Imaging.ImageFormat.Gif);
                                break;
                        }
                        fs.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: could not save the file. Error: " + ex.Message);
                }
            }
            

        }

        private void importToolStripMenuItem_Click(object sender, EventArgs e) // open image
        {
            if (!isProjectOpenned) { return; }
            using(OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Import an Image File";
                dlg.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
                if(dlg.ShowDialog() == DialogResult.OK)
                {
                    if(dlg.OpenFile() != null)
                    {
                        //Console.WriteLine("Image mp.AddImage() from Form1");
                        mp.AddImage(Image.FromFile(dlg.FileName), false);
                        
                    }
                }
            }
        

        }

        private void addImageToLayerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isProjectOpenned) { return; }
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Import an Image File";
                dlg.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    if (dlg.OpenFile() != null)
                    {
                        //Console.WriteLine("Image mp.AddImage() from Form1");
                        mp.AddImage(Image.FromFile(dlg.FileName), true);

                    }
                }
            }
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (!isProjectOpenned) { return; }
            isMousePressed = true;
            lastMouseClick = new Point(e.X, e.Y);
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (!isProjectOpenned) { return; }
            if (isMousePressed)
            {
                //if(selectedTool == tool_type.Move) { return; }
                mp.useTool(selectedTool,e.X,e.Y);
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (!isProjectOpenned) { return; }
            isMousePressed = false;
        }

        private void addFilter_Click(object sender, EventArgs e)
        {
            if (!isProjectOpenned) { return; }
            ToolStripItem t = sender as ToolStripItem;
            effect_type et = (effect_type)Enum.Parse(typeof(effect_type), (string)t.Tag);
            mp.addEffect(et);
        }

        private void CloseFormButton_Click(object sender, EventArgs e)
        {
            Close();
            Application.Exit();
        }

        private void MaximalizeFormButton_Click(object sender, EventArgs e)
        {
            if(WindowState == FormWindowState.Maximized)
            {
                WindowState = FormWindowState.Normal;
            }
            else
            {
                WindowState = FormWindowState.Maximized;
            }
            
        }

        private void MinimalizeFormButton_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void Form1ButtonPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Left)
            {
                FormBeingDragged = true;
                FormMouseX = e.X;
                FormMouseY = e.Y;
            }
        }

        private void Form1ButtonPanel_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                FormBeingDragged = false;
            }
        }

        private void Form1ButtonPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (FormBeingDragged)
            {
                Point p = new Point();
                p.X = this.Location.X + (e.X - FormMouseX);
                p.Y = this.Location.Y + (e.Y - FormMouseY);
                this.Location = p;
            }
        }

        private void Form1ButtonPanel_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (WindowState == FormWindowState.Maximized)
            {
                WindowState = FormWindowState.Normal;
            }
            else
            {
                WindowState = FormWindowState.Maximized;
            }
        }

        private class MyRenderer : ToolStripProfessionalRenderer
        {
            public MyRenderer() : base(new MyColors()) { }
        }

        private class MyColors : ProfessionalColorTable
        {
            public override Color MenuItemSelected
            {
                get { return Color.FromArgb(44, 62, 80); }
            }
            public override Color MenuItemSelectedGradientBegin
            {
                get { return Color.FromArgb(44,62, 80); }
            }
            public override Color MenuItemSelectedGradientEnd
            {
                get { return Color.FromArgb(44, 62, 80); }
            }
            public override Color MenuItemPressedGradientBegin
            {
                get { return Color.FromArgb(44, 62, 80); }
            }
            public override Color MenuItemPressedGradientEnd
            {
                get { return Color.FromArgb(44, 62, 80); }
            }
            public override Color MenuBorder
            {
                get { return Color.Transparent; }
            }
            public override Color MenuItemBorder
            {
                get { return Color.Transparent; }
            }
            public override Color ToolStripDropDownBackground 
            {
                get { return Color.FromArgb(44, 62, 80); }
            }
        }

        
    }
    
}

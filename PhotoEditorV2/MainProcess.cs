﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhotoEditorV2
{
    class MainProcess
    {
        /*?REFERENCE*/
        private Form1 form1res;
        private PictureBox pbres;
        private WindowToolbar wtres;

        /*?MAINPROCESS*/
        private List<Layer> layerList;
        private Layer selectedLayer = null;
        private int currentLayerId;

        /*?TOOLS*/
        private int brush_size = 3;
        private Color brush_color = Color.White;
        private int erase_size = 3;
        private int brush_opacity = 255;
        private int type_size = 5;
        private FontFamily font_type = new FontFamily("Times New Roman");
        

        public MainProcess(Form1 parent,PictureBox pictureparent,WindowToolbar wtparent)
        {
            this.form1res = parent;
            this.pbres = pictureparent;
            this.wtres = wtparent;
            init();
        }

        private void init()
        {
            layerList = new List<Layer>();
            currentLayerId = 0;
        }

        public void AddImage(Image i,bool addtolayer)
        {
            //Console.WriteLine("add image,addtolayer: {0} ", addtolayer);
            if (addtolayer) // add picture to existing layer
            {
                if(layerList.Count > 0) // if the are layer
                {
                    if(selectedLayer != null) // if the selected layer is not null
                    {
                        
                        selectedLayer.AddPicture(i);
                        Console.WriteLine("Image selectedLayer from MainProcess ADDED TO EXISTING LAYER");
                    }
                }
            }
            else // create new layer and insert it here
            {
                
                Layer nlr = new Layer(form1res, this, wtres);
                nlr.AddPicture(i);
                nlr.SelectLayer();
                layerList.Add(nlr);
                Console.WriteLine("Image selectedLayer from MainProcess NEW LAYER");
            }
            //! RENDER
            renderImage();

        }

        public void SelectLayer(int id)
        {
            if(layerList.Count > 0)
            {
                foreach(Layer l in layerList)
                {
                    if(l.LayerId == id)
                    {
                        l.SelectLayer();
                        selectedLayer = l;
                        break;
                    }
                }
            }
        }

        public void DeselectLayer()
        {
            selectedLayer = null;
        }

        public void NewLayer()
        {
            Layer nlr = new Layer(form1res, this, wtres);
            nlr.SelectLayer();
            layerList.Add(nlr);

            SelectLayer(nlr.LayerId);
            
            
        }
         
        public void DeleteLayer()
        {
            if(selectedLayer != null)
            {
                Layer sl = selectedLayer;
                if (layerList.Count > 0)
                {
                    foreach (Layer l in layerList)
                    {
                        if (l.LayerId == sl.LayerId)
                        {
                            wtres.deleteLayerPanel(sl.LayerId);
                            sl.OnDelete();
                            layerList.Remove(sl);
                            DeselectLayer();
                            renderImage();
                            break;
                            
                        }
                    }
                }
            }
            
        }

        private void renderImage()
        {
            Rectangle r = form1res.GetProjectSize();
            Bitmap finalb = new Bitmap(r.Width,r.Height);
            foreach (Layer l in layerList) // layered images save last layered
            {
                if (l.IsEmpty()) { continue; }
                if (!l.LayerEnabled) { continue; }
                Color p = new Color();
                Point m = l.LayerMoved;
                int offsetX = 0;
                int offsetY = 0;
                if(m.X != 0) { offsetX = m.X; }// -100
                if(m.Y != 0) { offsetY = m.Y; }// -100
                using(Graphics g = Graphics.FromImage(finalb))
                {
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

                    
                    ColorMatrix mtrx = new ColorMatrix();
                     mtrx.Matrix33 = (float)(l.LayerOpacity / 100);
                     ImageAttributes attributes = new ImageAttributes();
                     attributes.SetColorMatrix(mtrx, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
                     g.DrawImage(l.Rendered, new Rectangle(offsetX, offsetY, l.Rendered.Width, l.Rendered.Height), 0, 0, l.Rendered.Width, l.Rendered.Height, GraphicsUnit.Pixel, attributes);
                     

                    //this.Location.X + (e.X - FormMouseX)

                    //g.DrawImage(l.Rendered, new Point( (form1res.GetLastClickPosition().X + offsetX) ,( form1res.GetLastClickPosition().Y + offsetY) ));
                    //g.DrawImage(l.Rendered, offsetX, offsetY);
                    g.Save();
                    g.Dispose();
                }
            }

            pbres.Image = (Image)finalb.Clone();
            finalb.Dispose();
            GC.Collect();

        }

        public void newProject()
        {
            layerList.Clear();
            NewLayer();
            renderImage();
        }

        public void layerHideClicked(int id)
        {
            if (layerList.Count > 0)
            {
                foreach (Layer l in layerList)
                {
                    if (l.LayerId == id)
                    {
                        l.LayerEnabled = !l.LayerEnabled;
                        wtres.updateLayerHide(id, l.LayerEnabled);
                        renderImage();
                        break;
                    }
                }
            }
        }

        public void useTool(tool_type t,int x, int y)
        {
            if(selectedLayer == null) { return; }
            switch (t)
            {
                case tool_type.Paint:
                    selectedLayer.Paint(x, y);
                    break;
                case tool_type.Erase:
                    selectedLayer.Erase(x, y);
                    break;
                case tool_type.Move:
                    selectedLayer.moveLayer(x, y);
                    break;
                
            }
            renderImage();
        }

        public void addEffect(effect_type e)
        {
            if(layerList.Count > 0)
            {
                if (selectedLayer != null)
                {
                    selectedLayer.AddEffect(e);

                }
            }
            
        }

        public void removeEffect(int lid,int eid)
        {
            if (layerList.Count > 0)
            {
                foreach (Layer l in layerList)
                {
                    if (l.LayerId == lid)
                    {
                        l.RemoveEffect(eid);
                        renderImage();
                        break;
                    }
                }
            }
        }

        private void openUpdateEffect(int lid, int eid,effect_type t,Effect e)
        {//ColorFilter ,Gamma , Brightness, Contrast, Grayscale, Invert, Sepia, Quantize
            switch (t)
            {
                case effect_type.ColorFilter:
                    updateEffectColorFilter(lid, eid, t, e);
                    break;
                case effect_type.Gamma:
                    updateEffectGamma(lid, eid, t, e);
                    break;
                case effect_type.Brightness:
                    updateEffectBrightness(lid, eid, t, e);
                    break;
                case effect_type.Contrast:
                    updateEffectContrast(lid, eid, t, e);
                    break;
            }
        }

        private void updateEffectColorFilter(int lid, int eid, effect_type t, Effect e)
        {
            Form f = new Form();
            f.Width = 300;
            f.MaximizeBox = false;
            f.MinimizeBox = false;
            f.FormBorderStyle = FormBorderStyle.FixedSingle;
            f.StartPosition = FormStartPosition.CenterScreen;
            f.Text = t.ToString();

            int lastR = e.RgbFilter_r;
            int lastG = e.RgbFilter_g;
            int lastB = e.RgbFilter_b;

            FlowLayoutPanel cfflp = new FlowLayoutPanel();
            cfflp.Width = f.Width;
            cfflp.Dock = DockStyle.Fill;
            cfflp.FlowDirection = FlowDirection.TopDown;
            cfflp.Padding = Padding.Empty;
            cfflp.Margin = Padding.Empty;

            for (int i = 0; i < 3; i++)
            {
                string n = "";
                string n2 = "";
                string trackbarn = "";
                switch (i)
                {
                    case 0:
                        n = "panel_r";
                        n2 = "Red";
                        trackbarn = "trackbarr";
                        break;
                    case 1:
                        n = "panel_g";
                        n2 = "Green";
                        trackbarn = "trackbarg";
                        break;
                    case 2:
                        n = "panel_b";
                        n2 = "Blue";
                        trackbarn = "trackbarb";
                        break;
                }

                Panel cfpr = new Panel();
                cfpr.Height = 100;
                cfpr.Padding = Padding.Empty;
                cfpr.Margin = Padding.Empty;
                cfpr.Name = n;

                Label cfprn = new Label();
                cfprn.Height = 20;
                cfprn.Text = n2;
                cfprn.Dock = DockStyle.Top;

                Panel cfpr1 = new Panel();
                cfpr1.Height = 80;
                cfpr1.Padding = Padding.Empty;
                cfpr1.Margin = Padding.Empty;
                cfpr1.Dock = DockStyle.Bottom;

                TrackBar cftr = new TrackBar();
                cftr.Dock = DockStyle.Fill;
                cftr.Name = trackbarn;
                cftr.Minimum = -255;
                cftr.Maximum = 255;
                switch (i)
                {
                    case 0:
                        cftr.Value = lastR;
                        break;
                    case 1:
                        cftr.Value = lastG;
                        break;
                    case 2:
                        cftr.Value = lastB;
                        break;
                    default:
                        cftr.Value = 0;
                        break;
                }


                cfpr1.Controls.Add(cftr);
                cfpr.Controls.Add(cfprn);
                cfpr.Controls.Add(cfpr1);
                cfflp.Controls.Add(cfpr);
            }

            Button okbutton = new Button();
            okbutton.Height = 50;
            okbutton.Dock = DockStyle.Fill;
            okbutton.Text = "OK";
            okbutton.DialogResult = DialogResult.OK;

            cfflp.Controls.Add(okbutton);
            f.Controls.Add(cfflp);
            f.Height = 400;

            if (f.ShowDialog() == DialogResult.OK)
            {
                TrackBar cf_track_r = f.Controls.Find("trackbarr", true)[0] as TrackBar;
                TrackBar cf_track_g = f.Controls.Find("trackbarg", true)[0] as TrackBar;
                TrackBar cf_track_b = f.Controls.Find("trackbarb", true)[0] as TrackBar;

                FilterValue_ColorFilter v = new FilterValue_ColorFilter(cf_track_r.Value, cf_track_g.Value, cf_track_b.Value);

                updateEffect(lid, eid, v);
                renderImage();
            }

        }

        private void updateEffectGamma(int lid, int eid, effect_type t, Effect e)
        {
            Form f = new Form();
            f.Width = 300;
            f.MaximizeBox = false;
            f.MinimizeBox = false;
            f.FormBorderStyle = FormBorderStyle.FixedSingle;
            f.StartPosition = FormStartPosition.CenterScreen;
            f.Text = t.ToString();

            int lastR = (int)(e.Gamma_r * 10);
            int lastG = (int)(e.Gamma_g * 10);
            int lastB = (int)(e.Gamma_b * 10);

            FlowLayoutPanel cfflp = new FlowLayoutPanel();
            cfflp.Width = f.Width;
            cfflp.Dock = DockStyle.Fill;
            cfflp.FlowDirection = FlowDirection.TopDown;
            cfflp.Padding = Padding.Empty;
            cfflp.Margin = Padding.Empty;

            for (int i = 0; i < 3; i++)
            {
                string n = "";
                string n2 = "";
                string trackbarn = "";
                switch (i)
                {
                    case 0:
                        n = "panel_r";
                        n2 = "Red";
                        trackbarn = "trackbarr";
                        break;
                    case 1:
                        n = "panel_g";
                        n2 = "Green";
                        trackbarn = "trackbarg";
                        break;
                    case 2:
                        n = "panel_b";
                        n2 = "Blue";
                        trackbarn = "trackbarb";
                        break;
                }

                Panel cfpr = new Panel();
                cfpr.Height = 100;
                cfpr.Padding = Padding.Empty;
                cfpr.Margin = Padding.Empty;
                cfpr.Name = n;

                Label cfprn = new Label();
                cfprn.Height = 20;
                cfprn.Text = n2;
                cfprn.Dock = DockStyle.Top;

                Panel cfpr1 = new Panel();
                cfpr1.Height = 80;
                cfpr1.Padding = Padding.Empty;
                cfpr1.Margin = Padding.Empty;
                cfpr1.Dock = DockStyle.Bottom;

                TrackBar cftr = new TrackBar();
                cftr.Dock = DockStyle.Fill;
                cftr.Name = trackbarn;
                cftr.Minimum = 2;
                cftr.Maximum = 50;
                switch (i)
                {
                    case 0:
                        cftr.Value = lastR;
                        break;
                    case 1:
                        cftr.Value = lastG;
                        break;
                    case 2:
                        cftr.Value = lastB;
                        break;
                    default:
                        cftr.Value = 23;
                        break;
                }


                cfpr1.Controls.Add(cftr);
                cfpr.Controls.Add(cfprn);
                cfpr.Controls.Add(cfpr1);
                cfflp.Controls.Add(cfpr);
            }

            Button okbutton = new Button();
            okbutton.Height = 50;
            okbutton.Dock = DockStyle.Fill;
            okbutton.Text = "OK";
            okbutton.DialogResult = DialogResult.OK;

            cfflp.Controls.Add(okbutton);
            f.Controls.Add(cfflp);
            f.Height = 400;

            if (f.ShowDialog() == DialogResult.OK)
            {
                TrackBar cf_track_r = f.Controls.Find("trackbarr", true)[0] as TrackBar;
                TrackBar cf_track_g = f.Controls.Find("trackbarg", true)[0] as TrackBar;
                TrackBar cf_track_b = f.Controls.Find("trackbarb", true)[0] as TrackBar;

                FilterValue_Gamma v = new FilterValue_Gamma((cf_track_r.Value / 10), (cf_track_g.Value / 10), (cf_track_b.Value / 10));

                updateEffect(lid, eid, v);
                renderImage();
            }
        }

        private void updateEffectBrightness(int lid, int eid, effect_type t, Effect e)
        {
            Form f = new Form();
            f.Width = 300;
            f.MaximizeBox = false;
            f.MinimizeBox = false;
            f.FormBorderStyle = FormBorderStyle.FixedSingle;
            f.StartPosition = FormStartPosition.CenterScreen;
            f.Text = t.ToString();

            int lastV = e.Brightness_value;

            FlowLayoutPanel cfflp = new FlowLayoutPanel();
            cfflp.Width = f.Width;
            cfflp.Dock = DockStyle.Fill;
            cfflp.FlowDirection = FlowDirection.TopDown;
            cfflp.Padding = Padding.Empty;
            cfflp.Margin = Padding.Empty;

            Panel cfpr = new Panel();
            cfpr.Height = 100;
            cfpr.Padding = Padding.Empty;
            cfpr.Margin = Padding.Empty;
            cfpr.Name = "brightnesspanel";

            Label cfprn = new Label();
            cfprn.Height = 20;
            cfprn.Text = "Brightness";
            cfprn.Dock = DockStyle.Top;

            Panel cfpr1 = new Panel();
            cfpr1.Height = 80;
            cfpr1.Padding = Padding.Empty;
            cfpr1.Margin = Padding.Empty;
            cfpr1.Dock = DockStyle.Bottom;

            TrackBar cftr = new TrackBar();
            cftr.Dock = DockStyle.Fill;
            cftr.Name = "brightnesstrackbar";
            cftr.Minimum = -255;
            cftr.Maximum = 255;
            cftr.Value = lastV;

            cfpr1.Controls.Add(cftr);
            cfpr.Controls.Add(cfprn);
            cfpr.Controls.Add(cfpr1);
            cfflp.Controls.Add(cfpr);

            Button okbutton = new Button();
            okbutton.Height = 50;
            okbutton.Dock = DockStyle.Fill;
            okbutton.Text = "OK";
            okbutton.DialogResult = DialogResult.OK;

            cfflp.Controls.Add(okbutton);
            f.Controls.Add(cfflp);
            f.Height = 150;

            if (f.ShowDialog() == DialogResult.OK)
            {
                TrackBar bs_track_r = f.Controls.Find("brightnesstrackbar", true)[0] as TrackBar;

                FilterValue_Brightness v = new FilterValue_Brightness(bs_track_r.Value);

                updateEffect(lid, eid, v);
                renderImage();
            }
        }

        private void updateEffectContrast(int lid, int eid, effect_type t, Effect e)
        {
            Form f = new Form();
            f.Width = 300;
            f.MaximizeBox = false;
            f.MinimizeBox = false;
            f.FormBorderStyle = FormBorderStyle.FixedSingle;
            f.StartPosition = FormStartPosition.CenterScreen;
            f.Text = t.ToString();

            int lastV = e.Contrast_value;

            FlowLayoutPanel cfflp = new FlowLayoutPanel();
            cfflp.Width = f.Width;
            cfflp.Dock = DockStyle.Fill;
            cfflp.FlowDirection = FlowDirection.TopDown;
            cfflp.Padding = Padding.Empty;
            cfflp.Margin = Padding.Empty;

            Panel cfpr = new Panel();
            cfpr.Height = 100;
            cfpr.Padding = Padding.Empty;
            cfpr.Margin = Padding.Empty;
            cfpr.Name = "contrastpanel";

            Label cfprn = new Label();
            cfprn.Height = 20;
            cfprn.Text = "Contrast";
            cfprn.Dock = DockStyle.Top;

            Panel cfpr1 = new Panel();
            cfpr1.Height = 80;
            cfpr1.Padding = Padding.Empty;
            cfpr1.Margin = Padding.Empty;
            cfpr1.Dock = DockStyle.Bottom;

            TrackBar cftr = new TrackBar();
            cftr.Dock = DockStyle.Fill;
            cftr.Name = "contrasttrackbar";
            cftr.Minimum = -100;
            cftr.Maximum = 100;
            cftr.Value = lastV;

            cfpr1.Controls.Add(cftr);
            cfpr.Controls.Add(cfprn);
            cfpr.Controls.Add(cfpr1);
            cfflp.Controls.Add(cfpr);

            Button okbutton = new Button();
            okbutton.Height = 50;
            okbutton.Dock = DockStyle.Fill;
            okbutton.Text = "OK";
            okbutton.DialogResult = DialogResult.OK;

            cfflp.Controls.Add(okbutton);
            f.Controls.Add(cfflp);
            f.Height = 150;

            if (f.ShowDialog() == DialogResult.OK)
            {
                TrackBar bs_track_r = f.Controls.Find("contrasttrackbar", true)[0] as TrackBar;

                FilterValue_Brightness v = new FilterValue_Brightness(bs_track_r.Value);

                updateEffect(lid, eid, v);
                renderImage();
            }
        }

        public void updateEffect(int lid, int eid, effect_type t)
        {
            if (layerList.Count > 0)
            {
                foreach (Layer l in layerList)
                {
                    if (l.LayerId == lid)
                    {
                        Effect e = l.GetEffectById(eid);
                        if(e == null) { return; }
                        openUpdateEffect(lid, eid, t,e);
                        break;
                    }
                }
            }
        }

        private void updateEffect(int lid, int eid,object v)
        {
            if (layerList.Count > 0)
            {
                foreach (Layer l in layerList)
                {
                    if (l.LayerId == lid)
                    {
                        l.UpdateEffec(eid, v);
                        break;
                    }
                }
            }
        }

        public void turnEffect(int lid, int eid)
        {
            if (layerList.Count > 0)
            {
                foreach (Layer l in layerList)
                {
                    if (l.LayerId == lid)
                    {
                        l.TurnEffect(eid);
                        renderImage();
                        break;
                    }
                }
            }
        }

        public void updateLayerOpacity(int value)
        {
            if(selectedLayer != null)
            {
                foreach(Layer l in layerList)
                {
                    if(l.LayerId == selectedLayer.LayerId)
                    {
                        l.LayerOpacity = value;
                        renderImage();
                        break;
                    }
                }
            }
        }

        public int getLayerOpacity()
        {
            if(selectedLayer != null)
            {
                return selectedLayer.LayerOpacity;
            }
            else
            {
                return 0;
            }
        }

        public int GetNextLayerId()
        {
            return currentLayerId++;
        }

        public int Brush_size { get => brush_size; set => brush_size = value; }
        public Color Brush_color { get => brush_color; set => brush_color = value; }
        public int Erase_size { get => erase_size; set => erase_size = value; }
        public int Brush_opacity { get => brush_opacity; set => brush_opacity = value; }
        public int Type_size { get => type_size; set => type_size = value; }
        public FontFamily Font_type { get => font_type; set => font_type = value; }
    }
}

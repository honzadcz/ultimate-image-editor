# Ultimate image editor

![UID](https://i.imgur.com/bFahlpY.png)

Image editor wrote in C# with capability of simple image effects and painting.

## Project branches

The project it self is divided in 2 branches, Main for somewhat stable build and Test for experimental new versions.

### Installing

If instaling the Test branch, as first install included font from resources folder.
Then Simply clone one of the branches into Visual studio and build it.

## Versioning

## Authors

* **Jan Ďurďák** 

## License

[![MIT license](http://img.shields.io/badge/license-MIT-brightgreen.svg)](http://opensource.org/licenses/MIT)